#include <linux/init.h>
#include <linux/module.h>

#include <linux/ext3_jbd.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/time.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lorenzo Colitti <lorenzo@colitti.com>");

static void on_read(struct file *f, struct page *page) {
	char buf[256];
	char *name = NULL;
	struct timeval tv;

	do_gettimeofday(&tv);

	if(f && f->f_dentry && f->f_vfsmnt) {
		name = d_path(f->f_dentry, f->f_vfsmnt, buf, sizeof(buf) - 1);
	}

	printk(KERN_DEBUG "READ: %lu.%lu (%s/%d) %lu %s\n",
		tv.tv_sec, tv.tv_usec,
		current ? current->comm : "?",
		current ? current->pid : 0,
		page ? page->index : 999999,
		name ? name : "<no dentry>"
		);
}

static int __init iolog_init(void) {
	int ret;

	printk(KERN_INFO "Loading iolog module\n");

	ret = register_read_callback(on_read);
	if(ret != 0)
		printk(KERN_INFO "Unable to register callback: %d\n", ret);

	return ret;
}

static void __exit iolog_exit(void) {
	printk(KERN_INFO "Unloading iolog module\n");

	unregister_read_callback(on_read);
}

module_init(iolog_init);
module_exit(iolog_exit);
