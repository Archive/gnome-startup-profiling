#!/usr/bin/env python

from sys import argv, exit
from string import split, strip

class Read:
    def __init__(self, start, length):
        self.start = start
        self.length = length

    def toString(self):
        return "%d-%d" % (self.start, self.start + self.length - 1)

class Process:
    def __init__(self, name):
        self.name = name
        # Hash of list of reads to every file
        self.reads = {}

    def addRead(self, filename, offset):
        # Coalesce multiple sequential reads into one big read operation
        if self.reads.has_key(filename):
            # We have already seen reads from this file.
            lastread = self.reads[filename][-1]
            # Is this a sequential read or a seek?
            if lastread.start + lastread.length == offset:
                # Sequential, combine it with the previous read
                lastread.length += 1
            else:
                # Seek, create a new read
                self.reads[filename].append(Read(offset, 1))
        else:
            # This is the first read we see from this file
            self.reads[filename] = [ Read(offset, 1) ]

    def printReads(self):
        for filename in self.reads.keys():
            for read in self.reads[filename]:
                print "%s: %s %s" % (self.name, filename, read.toString())

def findprocess(processes, processinfo):
    for currentprocess in processes:
        if currentprocess.name == processinfo:
            return currentprocess
    return None

if len(argv) < 2:
    exit("Usage: %s <io.log>" % argv[0])

processes = []

### Read log file
fd = open(argv[1])

line = fd.readline()
while len(line) > 0:
    if not " READ: " in line:
        line = fd.readline()
        continue

    line = strip(line)
    readinfo = split(line, " READ: ", 2)[1]
    timestamp, processinfo, offset, filename = split(readinfo, " ", 4)

    currentprocess = findprocess(processes, processinfo)
    if currentprocess == None:
        currentprocess = Process(processinfo)
        processes.append(currentprocess)

    currentprocess.addRead(filename, int(offset))

    line = fd.readline()

### Output results
for currentprocess in processes:
    currentprocess.printReads()
