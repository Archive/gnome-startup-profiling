#!/bin/sh

if [ -z "$1" ] ; then
	echo "Usage: $0 <io.log> [topN]" >&2
	exit 1
fi

if [ -n "$2" ]; then
	num=$2
else
	num=15
fi

./analyzereads.py $1 | cut -d " " -f 2 | sort | uniq -c | sort -nr | head -$num
