#!/bin/sh

if [ -z "$1" ] || [ -z "$2" ]; then
	echo "Usage: $0 <io.log> <process> [topN]" >&2
	exit 1
fi

if [ -n "$3" ]; then
	num=$3
else
	num=15
fi

cat $1 | grep -E "\($2\/[0-9]{1,5}\)" | cut -d " " -f 10 | sort | uniq -c | sort -nr | head -$num
