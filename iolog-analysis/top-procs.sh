#!/bin/sh

if [ -z "$1" ]; then
	echo "Usage: $0 <io.log>" >&2
	exit 1
fi

cat $1 | sed -e 's/.*(//' -e 's/\/.*//' | sort | uniq -c | sort -nr | head -15
